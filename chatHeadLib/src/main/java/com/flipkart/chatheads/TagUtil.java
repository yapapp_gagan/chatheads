package com.flipkart.chatheads;

import android.util.Log;

/**
 * Created by gaganpreet on 22/8/17.
 */

public class TagUtil {


    private static boolean IS_TAG_X_POS_ALLOWED = false;
    private static boolean IS_TAG_LIFECYCLE_ALLOWED = false;
    private static boolean IS_TAG_CHATHEADS_LEFT_ALLOWED = false;
    private static boolean IS_TAG_INITIALISE_ALLOWED = false;
    private static boolean IS_TAG_ACTIVATED_ALLOWED = false;
    private static boolean IS_TAG_POINT_TO_ALLOWED = false;
    private static boolean IS_TAG_TOUCH_UP_ALLOWED = false;
    private static boolean IS_TAG_POINT_ALLOWED = false;

    public static void e(TAGS tag, String data, Class aClass) {

        boolean isAllowed = false;

        if (tag == TAGS.TAG_X_POS && IS_TAG_X_POS_ALLOWED) {
            isAllowed = true;
        }

        if (tag == TAGS.TAG_LIFECYCLE && IS_TAG_LIFECYCLE_ALLOWED) {
            isAllowed = true;
        }

        if (tag == TAGS.TAG_CHATHEADS_LEFT && IS_TAG_CHATHEADS_LEFT_ALLOWED) {
            isAllowed = true;
        }


        if (tag == TAGS.TAG_INITIALISE && IS_TAG_INITIALISE_ALLOWED) {
            isAllowed = true;
        }

        if (tag == TAGS.TAG_ACTIVATED && IS_TAG_ACTIVATED_ALLOWED) {
            isAllowed = true;
        }

        if (tag == TAGS.TAG_POINT_TO && IS_TAG_POINT_TO_ALLOWED) {
            isAllowed = true;
        }

        if (tag == TAGS.TAG_TOUCH_UP && IS_TAG_TOUCH_UP_ALLOWED) {
            isAllowed = true;
        }

        if (tag == TAGS.TAG_POINT && IS_TAG_POINT_ALLOWED) {
            isAllowed = true;
        }


        if (isAllowed) {

            if (aClass == null) {
                Log.e(tag.toString(), data);
            } else {
                Log.e(aClass.getCanonicalName() + "-" + tag.toString(), data);
            }
        }
    }

    public static void e(TAGS tag, String data) {
        e(tag, data, null);
    }


    public static enum TAGS {
        TAG_X_POS,
        TAG_LIFECYCLE,
        TAG_CHATHEADS_LEFT,
        TAG_INITIALISE,
        TAG_ACTIVATED,
        TAG_POINT_TO,
        TAG_POINT,
        TAG_TOUCH_UP
    }


}
