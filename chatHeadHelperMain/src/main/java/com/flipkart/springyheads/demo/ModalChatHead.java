package com.flipkart.springyheads.demo;

import android.graphics.Bitmap;

/**
 * Created by gaganpreet on 17/8/17.
 */

public class ModalChatHead {
    public String id;
    public int notifications;
    public Bitmap bitmap;

    public ModalChatHead(String id, int notifications, Bitmap imageResId) {
        this.id = id;
        this.notifications = notifications;
        this.bitmap = imageResId;
    }

}
