package com.flipkart.springyheads.demo;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class FloatingActivity extends Activity implements View.OnClickListener {

    private static final int REQUEST_PERMISSION_OVERLAY = 1234;
    private static final java.lang.CharSequence STR_PLEASE_FILL_THIS = "Please fill this";
    private static final String STR_SERVICE_NOT_BOUND = "Service not bound";
    private Button addButton;
    private Button removeButton;
    private Button removeAllButtons;
    private Button toggleButton;
    private ChatHeadService chatHeadService;
    private boolean bound;
    private EditText etId;
    private EditText etbadgeCount;

    /**
     * Defines callbacks for service binding, passed to bindService()
     */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            ChatHeadService.LocalBinder binder = (ChatHeadService.LocalBinder) service;
            chatHeadService = binder.getService();
            bound = true;
        }


        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            bound = false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupButtons();

        ImageView iv = (ImageView) findViewById(R.id.ivImage);

        iv.setImageDrawable(
                getCustomChatHeadDrawable
                        (new ModalChatHead
                                ("1", 9, BitmapFactory.decodeResource(getResources(), R.drawable.img1)
                                )
                        )
        );

    }


    @Override
    protected void onResume() {
        super.onResume();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, REQUEST_PERMISSION_OVERLAY);
            } else startService();
        } else {
            startService();
        }

    }

    private void startService() {
        Intent intent = new Intent(this, ChatHeadService.class);
        startService(intent);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    private void setupButtons() {
        setContentView(R.layout.activity_main);
        addButton = (Button) findViewById(R.id.add_head);
        removeButton = (Button) findViewById(R.id.remove_head);
        removeAllButtons = (Button) findViewById(R.id.remove_all_heads);
        toggleButton = (Button) findViewById(R.id.toggle_arrangement);
        etId = (EditText) findViewById(R.id.etId);
        etbadgeCount = (EditText) findViewById(R.id.etBadgeCount);
        addButton.setOnClickListener(this);
        removeButton.setOnClickListener(this);
        removeAllButtons.setOnClickListener(this);
        toggleButton.setOnClickListener(this);
    }


    int imageResIndex;

    @Override
    public void onClick(View v) {
        if (bound) {
            Bitmap drawable = null;
            imageResIndex = imageResIndex + 1;
            if (imageResIndex == 4) {
                imageResIndex = 1;
            }

            switch (imageResIndex) {
                case 1:
                    drawable = (BitmapFactory.decodeResource(getResources(), R.drawable.img1));
                    break;

                case 2:
                    drawable = (BitmapFactory.decodeResource(getResources(), R.drawable.img2));
                    break;


                case 3:
                    drawable = (BitmapFactory.decodeResource(getResources(), R.drawable.img3));
                    break;


            }
            if (v == addButton) {
                if (etId.getText().toString().length() == 0) {
                    etId.setError(STR_PLEASE_FILL_THIS);
                    return;
                }
                if (etbadgeCount.getText().toString().length() == 0) {
                    etbadgeCount.setError(STR_PLEASE_FILL_THIS);
                    return;
                }
                chatHeadService.addOrUpdate(new ModalChatHead(etId.getText().toString()
                        , Integer.parseInt(etbadgeCount.getText().toString()), drawable));
            } else if (v == removeButton) {
                if (etId.getText().toString().length() == 0) {
                    etId.setError(STR_PLEASE_FILL_THIS);
                    return;
                }
                chatHeadService.removeChatHead(etId.getText().toString());
            } else if (v == removeAllButtons) {
                chatHeadService.removeAllChatHeads();
            } else if (v == toggleButton) {
                chatHeadService.toggleArrangement();
            }
            etId.setText("");
            etbadgeCount.setText("");
        } else {
            Toast.makeText(this, STR_SERVICE_NOT_BOUND, Toast.LENGTH_SHORT).show();
        }
    }


    public Drawable getCustomChatHeadDrawable(ModalChatHead modalChatHead) {
        Bitmap original = modalChatHead.bitmap;
        Bitmap mask = BitmapFactory.decodeResource(getResources(), R.drawable.chat_bubble_left_);
        int mWidth = mask.getWidth();
        int mHeight = mask.getHeight();
        int imageWidth = original.getWidth();
        int imageHeight = original.getHeight();
        float scale = Math.max((float) mWidth / (float) imageWidth, (float) mHeight / (float) imageHeight);
        float newWidth = scale * ((float) original.getWidth());
        float newHeight = scale * ((float) original.getHeight());
        original = Bitmap.createScaledBitmap(original, Math.round(newWidth), Math.round(newHeight), false);
        //You can change original image here and draw anything you want to be masked on it.
        Bitmap result = Bitmap.createBitmap(mask.getWidth(), mask.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas tempCanvas = new Canvas(result);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        tempCanvas.drawBitmap(original, 0, 0, null);
        tempCanvas.drawBitmap(mask, 0, 0, paint);
        paint.setXfermode(null);
        Bitmap processedImage = result;
        View notification =
                ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.notification_layout, null);
        ImageView ivFace = (ImageView) notification.findViewById(R.id.ivFace);
        TextView tvNotification = (TextView) notification.findViewById(R.id.tvNotification);
        ivFace.setImageBitmap(processedImage);
        tvNotification.setText(9 + "+");
        notification.setDrawingCacheEnabled(true);
        notification.buildDrawingCache();
        notification.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        notification.layout(0, 0, notification.getMeasuredWidth(), notification.getMeasuredHeight());
        final Bitmap clusterBitmap = Bitmap.createBitmap(notification.getMeasuredWidth(),
                notification.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(clusterBitmap);
        notification.draw(canvas);
        return new BitmapDrawable(clusterBitmap);
    }


    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_PERMISSION_OVERLAY)
            startService();
    }


}
