package com.flipkart.springyheads.demo;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.flipkart.chatheads.TagUtil;
import com.flipkart.chatheads.ui.ChatHead;
import com.flipkart.chatheads.ui.ChatHeadArrangement;
import com.flipkart.chatheads.ui.ChatHeadListener;
import com.flipkart.chatheads.ui.ChatHeadViewAdapter;
import com.flipkart.chatheads.ui.MaximizedArrangement;
import com.flipkart.chatheads.ui.MinimizedArrangement;
import com.flipkart.chatheads.ui.container.DefaultChatHeadManager;
import com.flipkart.chatheads.ui.container.WindowManagerContainer;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class ChatHeadService extends Service {


    // Binder given to clients
    private final IBinder mBinder = new LocalBinder();
    private DefaultChatHeadManager<String> chatHeadManager;
    private WindowManagerContainer windowManagerContainer;
    private Map<String, View> viewCache = new HashMap<>();
    private Map<String, ModalChatHead> heads = new HashMap();
    private Map<String, ModalChatHead> pendingAddList = new HashMap();
    private Handler hArrangementChange = new Handler();
    private final long TIME_DIFF = 200;

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        windowManagerContainer = new WindowManagerContainer(this);
        chatHeadManager = new DefaultChatHeadManager(this, windowManagerContainer);
        chatHeadManager.setListener(chatHeadListener);
        chatHeadManager.setViewAdapter(new ChatHeadViewAdapter<String>() {
            @Override
            public View attachView(String key, ChatHead chatHead, ViewGroup parent) {
                TagUtil.e(TagUtil.TAGS.TAG_LIFECYCLE, "attachView");
                return new View(getApplicationContext());
            }

            @Override
            public void detachView(String key, ChatHead<? extends Serializable> chatHead, ViewGroup parent) {
                TagUtil.e(TagUtil.TAGS.TAG_LIFECYCLE, "detachView");
                View cachedView = viewCache.get(key);
                if (cachedView != null) {
                    parent.removeView(cachedView);
                }
            }

            @Override
            public void removeView(String key, ChatHead<? extends Serializable> chatHead, ViewGroup parent) {
                TagUtil.e(TagUtil.TAGS.TAG_LIFECYCLE, "removeView");
                View cachedView = viewCache.get(key);
                if (cachedView != null) {
                    viewCache.remove(key);
                    parent.removeView(cachedView);
                }
            }

            @Override
            public Drawable getChatHeadDrawable(String key) {
                TagUtil.e(TagUtil.TAGS.TAG_LIFECYCLE, "getChatHeadDrawable");
                TagUtil.e(TagUtil.TAGS.TAG_LIFECYCLE, "getChatHeadDrawable");
                ModalChatHead modalChatHead = heads.get(key) != null ? heads.get(key) : pendingAddList.get(key);
                return ChatHeadService.this.getCustomChatHeadDrawable(modalChatHead);
            }
        });

        chatHeadManager.setArrangement(MinimizedArrangement.class, null);

    }

    /**
     * In Springy Chat Head there were 4 chat heads added by default.
     * Removing the four chat heads caused problem.
     * So This methods add chat head temporarily for few millis then removes it.
     */
    private void addTemporaryChatHead() {
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.app_icon);
        bitmap = bitmap.createScaledBitmap(bitmap, 1, 1, false);
        ModalChatHead modalChatHead = new ModalChatHead("id", 0,
                bitmap
        );
        addOrUpdate(modalChatHead);
        removeAllChatHeads();
    }


    ChatHeadListener chatHeadListener = new ChatHeadListener<String>() {
        @Override
        public void onChatHeadAdded(String key) {
            ModalChatHead chatHead = pendingAddList.get(key);
            pendingAddList.remove(key);
            heads.put(key, chatHead);
        }

        @Override
        public void onChatHeadRemoved(String key, boolean userTriggered) {
            heads.remove(key);
        }

        @Override
        public void onChatHeadArrangementChanged(ChatHeadArrangement oldArrangement, ChatHeadArrangement newArrangement) {
            if (oldArrangement == null) {
                addTemporaryChatHead();
            } else {
                if (!chatHeadManager.getChatHeads().isEmpty()) {
                    TagUtil.e(TagUtil.TAGS.TAG_LIFECYCLE, "size > 0");
                    hArrangementChange.removeCallbacks(rArrangementChange);
                    hArrangementChange.postDelayed(rArrangementChange, TIME_DIFF);
                }
            }
        }

        @Override
        public void onChatHeadAnimateEnd(ChatHead chatHead) {
            TagUtil.e(TagUtil.TAGS.TAG_LIFECYCLE, "onChatHeadAnimateEnd");
        }

        @Override
        public void onChatHeadAnimateStart(ChatHead chatHead) {
            TagUtil.e(TagUtil.TAGS.TAG_LIFECYCLE, "onChatHeadAnimateStart");
        }
    };

    private Runnable rArrangementChange = new Runnable() {
        @Override
        public void run() {
            try {
                String key = chatHeadManager.getKeyOfSelectedChatHead();
                ModalChatHead modalChatHead = heads.get(key);
                addOrUpdate(modalChatHead);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    public int getTotalBadgeCount() {
        int total = 0;
        for (Map.Entry<String, ModalChatHead> entry : heads.entrySet()) {
            total = total + (entry.getValue().notifications);
        }
        return total;
    }

    private String getCalculatedBadgeCount(ModalChatHead modalChatHead) {
        String heroKey = "";


        if (!chatHeadManager.getChatHeads().isEmpty())
            heroKey = chatHeadManager.getKeyOfSelectedChatHead();

        int badgeCount = modalChatHead.notifications;
        if (heads.size() > 0
                && heroKey.length() > 0
                &&
                heroKey.equals(modalChatHead.id)
                && (chatHeadManager.getActiveArrangement() instanceof MinimizedArrangement)) {
            badgeCount = getTotalBadgeCount();
        }

        Log.e("badgeCount", badgeCount > 9 ? 9 + "+" : Integer.toString(badgeCount));
        return badgeCount > 9 ? 9 + "+" : Integer.toString(badgeCount);
    }

    public Drawable getCustomChatHeadDrawable(ModalChatHead modalChatHead) {


        Bitmap original = modalChatHead.bitmap;
        Bitmap mask = BitmapFactory.decodeResource(getResources(), R.drawable.chat_bubble_left_);
        int mWidth = mask.getWidth();
        int mHeight = mask.getHeight();
        int imageWidth = original.getWidth();
        int imageHeight = original.getHeight();
        float scale = Math.max((float) mWidth / (float) imageWidth, (float) mHeight / (float) imageHeight);
        float newWidth = scale * ((float) original.getWidth());
        float neHeight = scale * ((float) original.getHeight());
        original = Bitmap.createScaledBitmap(original, Math.round(newWidth), Math.round(neHeight), false);

        //You can change original image here and draw anything you want to be masked on it.
        Bitmap result = Bitmap.createBitmap(mask.getWidth(), mask.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas tempCanvas = new Canvas(result);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        tempCanvas.drawBitmap(original, 0, 0, null);
        tempCanvas.drawBitmap(mask, 0, 0, paint);
        paint.setXfermode(null);

        Bitmap processedImage = result;

        View notification =
                ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.notification_layout, null);


        ImageView ivFace = (ImageView) notification.findViewById(R.id.ivFace);
        TextView tvNotification = (TextView) notification.findViewById(R.id.tvNotification);
        ivFace.setImageBitmap(processedImage);
        tvNotification.setText(getCalculatedBadgeCount(modalChatHead));


        notification.setDrawingCacheEnabled(true);
        notification.buildDrawingCache();


        notification.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        notification.layout(0, 0, notification.getMeasuredWidth(), notification.getMeasuredHeight());

        final Bitmap clusterBitmap = Bitmap.createBitmap(notification.getMeasuredWidth(),
                notification.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(clusterBitmap);
        notification.draw(canvas);

        return new BitmapDrawable(clusterBitmap);

    }

    public void addOrUpdate(final ModalChatHead modalChatHead) {
        String key = "";

        if (!chatHeadManager.getChatHeads().isEmpty())
            key = chatHeadManager.getKeyOfSelectedChatHead();

        if (heads.get(modalChatHead.id) == null) {
            addChatHead(modalChatHead);
        } else {
            update(modalChatHead, true);
        }
        if (key.length() > 0)
            update(heads.get(key), false);
        update(modalChatHead, true);
    }


    private void addChatHead(final ModalChatHead modalChatHead) {

        pendingAddList.put(modalChatHead.id, modalChatHead);

        ChatHead chatHead =
                chatHeadManager.addChatHead(modalChatHead.id, false, true);
        View.OnClickListener onChatHeadListener = new View.OnClickListener() {
            String s = modalChatHead.id;

            @Override
            public void onClick(View view) {
                Toast.makeText(ChatHeadService.this, "" + s, Toast.LENGTH_SHORT).show();
                removeChatHead(s);
                chatHeadManager.setArrangement(MinimizedArrangement.class, null);
            }
        };

        chatHead.setOnClickListener(onChatHeadListener);
        chatHeadManager.bringToFront(chatHeadManager.findChatHeadByKey(modalChatHead.id));
    }

    public void removeChatHead(ModalChatHead modalChatHead) {
        chatHeadManager.removeChatHead(modalChatHead.id, true);
    }


    public void removeChatHead(String key) {
        chatHeadManager.removeChatHead(String.valueOf(key), true);
    }


    public void removeAllChatHeads() {
        chatHeadManager.removeAllChatHeads(true);
    }

    public void toggleArrangement() {
        if (chatHeadManager.getActiveArrangement() instanceof MinimizedArrangement) {
            chatHeadManager.setArrangement(MaximizedArrangement.class, null);
        } else {
            chatHeadManager.setArrangement(MinimizedArrangement.class, null);
        }
    }


    public void update(ModalChatHead modalChatHead, boolean bringToFront) {
        heads.put(modalChatHead.id, modalChatHead);
        chatHeadManager.reloadDrawable(modalChatHead.id);

        if (bringToFront)
            chatHeadManager.bringToFront(chatHeadManager.findChatHeadByKey(modalChatHead.id));
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        windowManagerContainer.destroy();
    }

    public void minimize() {
        TagUtil.e(TagUtil.TAGS.TAG_LIFECYCLE, "Arrangement Minimized");
        chatHeadManager.setArrangement(MinimizedArrangement.class, null);
    }

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        ChatHeadService getService() {
            // Return this instance of LocalService so clients can call public methods
            return ChatHeadService.this;
        }
    }
}